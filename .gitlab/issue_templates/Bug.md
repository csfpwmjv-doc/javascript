<!-- Veuillez fournir un résumé de l'erreur dans le titre ci-dessus. -->

## Emplacement
<!-- Complétez l'URL ci-desous afin de pointer vers la page où est l'erreur.-->

[Consulter la page](https://csfpwmjv-doc.gitlab.io/mobile1-doc/restant-de-url#section-au-besoin)

## Nature de l'erreur
<!--- Décrivez l'erreur. -->

## Solution possible
<!--- Ce n'est pas obligatoire, mais vous pouvez suggérer un correctif. -->

/label ~Bug