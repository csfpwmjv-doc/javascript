const siteConfig = {
  title: 'JavaScript',
  tagline: 'Portail du cours de JavaScript',
  url: 'https://csfpwmjv.gitlab.io/',
  baseUrl: '/javascript/',
  projectName: 'site',
  organizationName: 'Cégep de Sainte-Foy',
  headerLinks: [
    {doc: 'f1', label: 'Notes de cours'},
  ],
  headerIcon: 'img/ic_icon.svg',
  footerIcon: 'img/ic_icon.svg',
  favicon: 'img/favicon.ico',
  colors: {
    primaryColor: '#3a3a3a',
    secondaryColor: '#3a3a3a',
  },
  copyright: `Copyright © ${new Date().getFullYear()} Benjamin Lemelin`,
  highlight: {
    theme: 'androidstudio' //Highlight.js theme
  },
  markdownPlugins: [
    function externalLinksIntoNewTab(md) {
      md.renderer.rules.link_open = (function () {
        let original = md.renderer.rules.link_open;
        return function () {
          let link = original.apply(this, arguments);
          if (link.includes("http"))
            return link.substring(0, link.length - 1) + ' target="_blank" rel="noreferrer noopener">';
          else
            return link
        };
      })();
    },
  ],
  scripts: [], //Custom scripts here that would be placed in <script> tags.
  stylesheets: [], //Custom stylesheets.
  onPageNav: 'separate', //Show right navigation on a documentation page.
  cleanUrl: true, // No .html extensions for paths.
};

module.exports = siteConfig;
